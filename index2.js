"use strict"
// 2. Напишіть програму, яка запитує в користувача число та перевіряє, 
// чи воно є парним числом. Якщо введене значення не є парним числом, 
// то запитуйте число доки користувач не введе правильне значення.

console.log("TASK 2");

let userNumber;

do {
    userNumber = prompt("Enter your number");
} while (isNaN(userNumber) || userNumber % 2 !== 0) {

    if (userNumber === null || userNumber === "") {
        console.log("You didn't enter a number");
    }

    if (userNumber) {
        console.log("Ви ввели парне число");
    }
}


